import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Heroe } from '../../servicios/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styles: [
  ]
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() heroe: Heroe; // Padre a Hijo
  @Input() indice: number; // Padre a Hijo

  @Output() heroeSeleccionado: EventEmitter<number>; // Hijo a Padre

  constructor( private _router: Router) {
    this.heroeSeleccionado= new EventEmitter();
   }

  ngOnInit(): void {
  }

  verHeroe(): void{
  //   // console.log('indice: ' + this.indice) ;
  this._router.navigate( ['/heroe', this.indice] );
    // this.heroeSeleccionado.emit( this.indice );
  }

}
