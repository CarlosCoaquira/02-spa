import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../servicios/heroes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html'
})
export class BuscadorComponent implements OnInit {

  heroes: Heroe[] = [];
  currentYear: Date = new Date();
  termino: string;

  constructor( private _activatedRoute: ActivatedRoute,
    private _heroeService: HeroesService) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe( params =>  {
      // console.log('HeroeComponent: ' );
      this.termino = params['termino'] ;

      this.heroes = this._heroeService.buscarHeroes( params[ 'termino' ] );

      console.log( this.heroes );
    } );
  }

}
